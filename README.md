## HARDWARE AND SOFTWARE REQUIREMENTS
# Device with 2 physical network interfaces
 * Raspberry pi 3 B+ for example (RJ45 and wlan)
# Linux based operating system with tc package installed.
 * Raspbian https://www.raspberrypi.org/downloads/raspbian/ 
# See tc man pages for more info https://linux.die.net/man/8/tc
# Manually set up wlan ap or prebuilt package such as RaspAP
 * https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md
# Root priviledges
1. Install and setup your operating system of choice
2. Setup your device as wireless access-point or wired gateway.
    * One WAN interface for incoming traffic.
    * One LAN interface for outgoing emulated traffic.
3. Copy netem files on emulation device.
4. Edit root crontab with "crontab -e" and add following lines to crontab and reboot
# SET PATHS IN ORDER TO EXEC COMMANDS.
PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin
LD_LIBRARY_PATH=/usr/local/lib
# LAUNCH SHELLSCRIPT AT STARTUP add following to crontab
@reboot /root/networksim/launchapi.sh