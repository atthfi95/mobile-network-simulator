package internal

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/julienschmidt/httprouter"
)

const (
	srcDir = "./src"
)

var (
	hname  string = "raspap" // Hostname
	domain string = "0.0.0.0"
	port   string = "8443"
	msg    string = "Emulation can be adjusted at http://" + hname + ":" + port + "/index.html OR via API http://" + hname + ":" + port + "/api/dsl or /set/gprs etc"
)

type templateContext struct {
	Data []string
}
type globalState struct {
	lock sync.RWMutex
	Data []string
}

// NetemAPI spawns webserver and commands netem according to requests.
func NetemAPI() { // LEGACY GET SOLUTION
	fmt.Println("Network emulation API launched.")
	fmt.Println(msg)

	router := httprouter.New()
	router.GET("/api/:command", apiRoute)
	router.GET("/index.html", baseRoute)
	router.GET("/index", baseRoute)
	router.GET("/", baseRoute)
	log.Fatal(http.ListenAndServe(domain+":"+port, router)) // Change to localhost if running local.
}

// API sequests in format: domain:8443/set/dsl/ or /set/gprs etc.
func apiRoute(w http.ResponseWriter, router *http.Request, p httprouter.Params) {
	var command = p.ByName("command")

	switch strings.ToLower(command) {
	case "exit", "quit", "/q", "\\q":
		// Override exit command with reset when using API
		command = "reset"
	}
	answer, _, err := Execute(command)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadGateway)
		return
	}

	temp := []byte("")

	w.Write([]byte(append([]byte(answer), temp...)))
	fmt.Println("Served apiRoute request: " + command)
}

func baseRoute(w http.ResponseWriter, router *http.Request, p httprouter.Params) {
	index, _ := ioutil.ReadFile("/root/networksim/internal/src/gui.html")

	w.Write([]byte(index))
	fmt.Println("Served baseRoute request")
}
