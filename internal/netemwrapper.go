package internal

import (
	"fmt"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
)

var (
	osystem string = runtime.GOOS // tc qdisc netem is linux only.
	Device         = "wlan0"      // Output interface, applying "add" on input interface will affect host system network.
	user           = "root"       // Always root, interface "add" not possible in user mode.
	p              = "25%"        // Netmen latency calculation
)

func Execute(command string) (answer string, flag bool, err error) {
	runFlag := true

	switch strings.ToLower(command) {
	case "reset": // Reset active values.
		err = ResetEmulation()
		answer = "Network emulation reset."
	case "swap", "switch", "swapif", "switchif":
		if Device == "wlan0" {
			Device = "eth0"
		} else {
			Device = "wlan0"
		}
		answer = "Emulation reset and interface changed to " + Device

	case "b2g", "bgprs", "bgsm": // Set gprs reference values. 700, 350, 3, 1, 50, "GPRS"
		err = set(1000, 600, 5, 2, 25, 0, "Bad GPRS")
		answer = "Very poor quality GPRS (2G) emulation set."
	case "b3g", "bhsdpa": // Set hsdpa reference values.
		err = set(300, 100, 2, 1, 750, 0, "Bad HSDPA (3G)")
		answer = "Very poor quality HSDPA (3G) emulation set."

	case "2g", "gprs", "gsm": // Set gprs reference values. 700, 350, 3, 1, 50, "GPRS"
		err = set(700, 350, 3, 1, 50, 0, "GPRS")
		answer = "GPRS (2G) emulation set."
	case "3g", "hsdpa": // Set hsdpa reference values.
		err = set(300, 100, 2, 1, 750, 0, "HSDPA (3G)")
		answer = "HSDPA (3G) emulation set."

	case "good", "auto", "on", "wlan", "wifi":
		err = set(60, 1, 1, 0, 10000, 0, "Good internet")
		answer = "OK Wifi emulation set."
	case "dsl", "adsl": // Set bad quality dsl reference values.
		err = set(500, 50, 2, 1, 900, 0, "Poor adsl")
		answer = "Bad quality adsl emulation set."

	case "satellite", "sat": // Set satellite reference values.
		err = set(6000, 1500, 5, 2, 5000, 0, "Satellite")
		answer = "Satellite emulation set."
	case "brokenaf", "broken", "shit":
		err = set(369, 33, 50, 5, 10000, 25, "Broken network with high packet loss")
		answer = "Broken network emulation set. High packet loss and corruption."

	case "exit", "quit", "/q", "\\q":
		ResetEmulation()
		runFlag = false
		answer = "Netem reset. Exiting shell."
	case "man", "help", "h": // View all avaiable commands.
		answer = "No help yet."
	default:
		answer = "Invalid command: " + command
	}
	if err != nil {
		answer = fmt.Sprintf("Failed due: %v", err)
	}
	return answer, runFlag, err
}

// ResetEmulation clears tc qdisc emulation devices.
func ResetEmulation() error { // Removes device.
	cmd := exec.Command("tc", "qdisc", "del", "dev", Device, user) // Delete current emulator if any.
	cmd.Run()
	return nil // Ignore errors.
}

// set handles local variable conversions and executes final command.
// TODO: convert to struct, this code is terrible.
func set(maxlagint, minlagint, lossceilingint, lossfloorint, bandwidthint, corruptint int, emulationType string) error { // Parse values to valid netem commands & execute.
	ResetEmulation()

	maxlag := strconv.Itoa(maxlagint) + "ms"
	minlag := strconv.Itoa(minlagint) + "ms"
	lossfloor := strconv.Itoa(lossfloorint) + "%"
	lossceiling := strconv.Itoa(lossceilingint) + "%"
	bandwidth := strconv.Itoa(bandwidthint) + "kbps"
	corrupt := strconv.Itoa(corruptint) + "%"

	args := []string{"qdisc", "add", "dev", Device, user, "netem", "delay", maxlag, minlag, p, "loss", lossfloor, lossceiling, "rate", bandwidth, "corrupt", corrupt}
	_, err := exec.Command("tc", args...).CombinedOutput()
	if err != nil {
		fmt.Println("tc", args)
	} else {
		fmt.Printf(user, "applied", emulationType, "on", Device, "\n")
	}
	return err
}
