package main

import (
	"flag"
	"fmt"
	"runtime"

	"gitlab.com/atthfi95/mobile-network-simulator/internal"
)

var (
	// APIServerFlag runs netem in webserver mode. False spawns shell.
	APIServerFlag bool
	osystem       string
	prompt        string
	answer        string
	user          string
	run           bool
	err           error
)

func init() {
	run = true
	user = "root" // root priviledges required to use netem.
	prompt = osystem + "/" + user + "/" + internal.Device + ">>"
	osystem = runtime.GOOS
}

// You can use netem wrapper via webui with -server flag instead of local shell.
func main() {
	internal.ResetEmulation() // Clean active emulators at the beginning.
	flag.BoolVar(&APIServerFlag, "server", false, "True to use server")
	flag.Parse()

	if APIServerFlag {
		internal.NetemAPI()
	} else {
		fmt.Println("Network emulation shell")
		fmt.Println("Type help to view command presets.")

		for run && !APIServerFlag {
			run = true
			input := ""
			if osystem != "linux" {
				fmt.Printf("\nNetem linux kernel module not found!\n")
				// TODO: Search valid netem gateway internal.Device from local network and check if this software is present there.
				// If netem wrapper found establish link.
				run = false
			} else {
				fmt.Printf(prompt)
				fmt.Scanf("%v", &input)
				fmt.Println("")
				answer, run, err = internal.Execute(input)
				fmt.Println(answer)
			}
		}
	}
}
